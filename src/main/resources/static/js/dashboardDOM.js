function taskTemplate1(name, desc, id) {
    return '<div id="' + id + '" class="row">' + '<div class="col-lg-12"">'
        + '<div class="roundedrec4">'
        + '<div class="insiderec4">'
        + '<p class="taskName">' + name + '</p>'
        + '<p class="taskDesc">' + desc + '</p>'
        + '<div class="kanan">'
        + '<button class="button2" onclick="deleteTaskRequest('+id+')"><i class="fas fa-trash"></i></button>'
        + '<button class="button2" onclick="showEditTaskModal('+id+')"><i class="fas fa-edit"></i></button>'
        + '<button class="button1" type="button" disabled><i class="fas fa-hand-point-left"></i></button>'
        + '<button class="button1" type="button" onclick="moveTaskRequest(' + id + ',' + "'ONDOING'" + ')"><i class="fas fa-hand-point-right"></i></button>'
        + '</div></div></div></div></div>'
}

function buttonTemplate(id, state) {
    if (state === 'TODO') {
        return '<button class="button1" type="button" disabled><i class="fas fa-hand-point-left"></i></button>'
            + '<button class="button1" type="button" onclick="moveTaskRequest(' + id + ',' + "'ONDOING'" + ')"><i class="fas fa-hand-point-right"></i></button>';
    } else if (state === 'ONDOING') {
        return '<button class="button1" type="button" onclick="moveTaskRequest(' + id + ',' + "'TODO'" + ')"><i class="fas fa-hand-point-left"></i></button>'
            + '<button class="button1" type="button" onclick="moveTaskRequest(' + id + ',' + "'DONE'" + ')"><i class="fas fa-hand-point-right"></i></button>';
    } else if (state === 'DONE') {
        return '<button class="button1" type="button" onclick="moveTaskRequest(' + id + ',' + "'ONDOING'" + ')"><i class="fas fa-hand-point-left"></i></button>'
            + '<button class="button1" type="button" disabled><i class="fas fa-hand-point-right"></i></button>';
    }
}

function showEditTaskModal(id) {
    var themodal = $("#editTask");
    console.log('changing default value');
    var nameinput = themodal.find('#newTaskNameInput').eq(0);
    var descinput = themodal.find('#newTaskDescriptionInput').eq(0);
    var modalid = themodal.find('#modalid').eq(0);
    console.log(nameinput);
    console.log(descinput);

    var taskC = $("#"+id);
    var name = taskC.find('.taskName').eq(0).text();
    var desc = taskC.find('.taskDesc').eq(0).text();
    nameinput.text(name);
    descinput.text(desc);
    modalid.text(id);
    themodal.modal();
}

function modalSubmit(){
    var themodal = $("#editTask");
    themodal.modal('hide');
    var name = themodal.find('#newTaskNameInput').eq(0).val();
    var desc = themodal.find('#newTaskDescriptionInput').eq(0).val();
    var id = themodal.find('#modalid').eq(0).val();
    console.log(name+desc+id);
    editTaskRequest(id, name, desc);
}

function addTask(name, desc, id) {
    var todocol = document.getElementById("todocol");
    console.log('masuk addTask');
    todocol.insertAdjacentHTML('beforeend', taskTemplate1(name, desc, id));
}

function moveTask(id, state) {
    var parent = $("#" + id);
    if (state === 'TODO') {
        parent.appendTo($("#todocol"));
    } else if (state === 'ONDOING') {
        parent.appendTo($("#ondoingcol"));
    } else if (state === 'DONE') {
        parent.appendTo($("#donecol"));
    }

    var kanan = parent.find('.kanan').eq(0);
    var button1 = kanan.find(".button1")[0];
    var button2 = kanan.find(".button1")[1];
    button1.remove();
    button2.remove();
    kanan.append(buttonTemplate(id, state))
}

function deleteTask(id) {
    var task = $("#" + id);
    task.remove();
}

function editTask(id, name, desc) {
    var taskC = $("#"+id);
    taskC.find('.taskName').eq(0).text(name);
    taskC.find('.taskDesc').eq(0).text(desc);
}