var stompClient = null;
connect();

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    var socket = new SockJS('/ontrack-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/greetings', function (greeting) {
            showGreeting(JSON.parse(greeting.body).content);
        });
        stompClient.subscribe('/board/addTaskResponse', function (request) {
            request = JSON.parse(request.body);
            if (request.boardName === boardName) {
                addTask(request.taskName, request.taskDesc, request.taskId);
            }
        });
        stompClient.subscribe('/board/moveTaskResponse', function (request) {
            request = JSON.parse(request.body);
            if (request.boardName === boardName) {
                moveTask(request.taskId, request.taskState);
            }
        });
        stompClient.subscribe('/board/deleteTaskResponse', function (request) {
            request = JSON.parse(request.body);
            if (request.boardName === boardName) {
                deleteTask(request.taskId);
            }
        });
        stompClient.subscribe('/board/editTaskResponse', function (request) {
            request = JSON.parse(request.body);
            if (request.boardName === boardName) {
                editTask(request.taskId, request.taskName, request.taskDesc);
            }
        });
    });
}

function addTaskRequest() {
    console.log("requesting add task");
    stompClient.send("/app/addTask", {},
        JSON.stringify(
            {
                'boardName': boardName,
                'taskName': document.getElementById('taskNameInput').value,
                'taskDesc': document.getElementById('taskDescriptionInput').value
            }
        ));
}

function moveTaskRequest(id, state) {
    console.log("requesting move task");
    stompClient.send("/app/moveTask", {},
        JSON.stringify(
            {
                'boardName': boardName,
                'taskId': id,
                'taskState': state
            }
        ));
}

function deleteTaskRequest(id) {
    console.log("requesting delete task");
    stompClient.send("/app/deleteTask", {},
        JSON.stringify(
            {
                'boardName': boardName,
                'taskId': id,
            }
        ));
}

function editTaskRequest(id, name, desc) {
    console.log("requesting edit task");
    stompClient.send("/app/editTask", {},
        JSON.stringify(
            {
                'boardName': boardName,
                'taskId': id,
                'taskName': name,
                'taskDesc': desc
            }
        ));
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendName() {
    stompClient.send("/app/hello", {}, JSON.stringify({'name': $("#name").val()}));
}

function showGreeting(message) {
    $("#greetings").append("<tr><td>" + message + "</td></tr>");
}

function saveEmail(){
    console.log("saving the email")
    var email = document.getElementById('emailInput').value;
    console.log(email);

    var hostname = window.location.hostname
    if (hostname==='localhost'){
        hostname = 'localhost:8080'
    }
    var url = "http://"+hostname+"/board/"+boardName+"/summary";
    console.log(url);
    $.get(url, function(data, status){
        var message = data;
        sendMail(email, message, "onTrack Board Summary");
    });
}

function sendMail(email, message, subject){
    $("#loader").css("display", "initial");
    $.ajax({
        url : "http://ontrack-mail.herokuapp.com/sendMail",
        data : {
        "emailTo":email,
        "textMessage":message,
        "subject":subject
        },
        success : function () {
            document.getElementById('emailInput').value = '';
            $("#loader").css("display", "none");
            $("#checked").css("display", "block");
        },
        error : function (xhr, status, error) {
            alert(xhr.status+": "+xhr.statusText);
        },
    });
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendName(); });
    $( "#taskForm" ).submit(function() { addTaskRequest(); });
    $( "#emailForm" ).submit(function() { saveEmail(); });
});