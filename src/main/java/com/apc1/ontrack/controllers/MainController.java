package com.apc1.ontrack.controllers;

import com.apc1.ontrack.models.*;
import com.apc1.ontrack.services.BasicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MainController {

    @Autowired
    BasicService service;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {
        return "home";
    }

    @RequestMapping("/board/{boardName}")
    public String board(@PathVariable("boardName") String boardName,
                             Model model) {
        Board board = service.getBoard(boardName);
        model.addAttribute("boardName", board.getName());
        model.addAttribute("tasks", board.getTasks());
        return "board";
    }

    @RequestMapping("/board/{boardName}/summary")
    @ResponseBody
    public String getSummary(@PathVariable("boardName") String boardName){
        Board board = service.getBoard(boardName);
        return board.getSummary();
    }

    @MessageMapping("/addTask")
    @SendTo("/board/addTaskResponse")
    public AddTaskResponseMessage addTask(AddTaskRequestMessage message){
        Board board = service.getBoard(message.getBoardName());
        Task newTask = board.addTask(message.getTaskName(), message.getTaskDesc());
        return new AddTaskResponseMessage(board, newTask);
    }

    @MessageMapping("/moveTask")
    @SendTo("/board/moveTaskResponse")
    public MoveTaskResponseMessage moveTask(MoveTaskRequestMessage message) {
        Board board = service.getBoard(message.getBoardName());
        Task task = board.getTask(message.getTaskId());
        task.setState(message.getTaskState());
        return new MoveTaskResponseMessage(board, task);
    }

    @MessageMapping("/deleteTask")
    @SendTo("/board/deleteTaskResponse")
    public DeleteTaskResponseMessage deleteTask(DeleteTaskRequestMessage message) {
        Board board = service.getBoard(message.getBoardName());
        board.removeTask(message.getTaskId());
        return new DeleteTaskResponseMessage(board, message.getTaskId());
    }

    @MessageMapping("/editTask")
    @SendTo("/board/editTaskResponse")
    public EditTaskResponseMessage editTask(EditTaskRequestMessage message) {
        System.out.println("edit task");
        String newName = message.getTaskName();
        System.out.println("new name: "+newName);
        String newDesc = message.getTaskDesc();
        System.out.println("new desc: "+newDesc);
        Integer taskId = message.getTaskId();
        Board board = service.getBoard(message.getBoardName());
        Task task = board.getTask(taskId);
        task.setName(newName);
        task.setDesc(newDesc);
        System.out.println("task.name"+task.getName());
        System.out.println("task.desc"+task.getDesc());
        return new EditTaskResponseMessage(board, task);
    }
}
