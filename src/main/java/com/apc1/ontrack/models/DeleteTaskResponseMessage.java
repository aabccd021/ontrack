package com.apc1.ontrack.models;

public class DeleteTaskResponseMessage {
    private String boardName;
    private Integer taskId;


    public DeleteTaskResponseMessage(Board board, Integer taskId) {
        this.boardName = board.getName();
        this.taskId = taskId;
    }

    public String getBoardName() {
        return boardName;
    }

    public Integer getTaskId() {
        return taskId;
    }

}
