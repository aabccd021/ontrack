package com.apc1.ontrack.models;

public class AddTaskResponseMessage {
    private String boardName;
    private String taskName;
    private String taskDesc;
    private Integer taskId;

    public AddTaskResponseMessage(Board board, Task newTask) {
        this.boardName = board.getName();
        this.taskName = newTask.getName();
        this.taskDesc = newTask.getDesc();
        this.taskId = newTask.getId();
    }

    public String getBoardName() {
        return boardName;
    }

    public String getTaskName() {
        return taskName;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public Integer getTaskId() {
        return taskId;
    }
}
