package com.apc1.ontrack.models;

public class DeleteTaskRequestMessage {
    private String boardName;
    private Integer taskId;

    public DeleteTaskRequestMessage(){}

    public DeleteTaskRequestMessage(String boardName, Integer taskId) {
        this.boardName = boardName;
        this.taskId = taskId;
    }

    public String getBoardName() {
        return boardName;
    }

    public Integer getTaskId() {
        return taskId;
    }
}
