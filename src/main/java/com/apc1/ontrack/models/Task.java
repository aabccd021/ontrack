package com.apc1.ontrack.models;

public class Task {
    private String state = "TODO";
    private String name;
    private Integer id;
    private String description;

    public Task(String name, String description, Integer id){
        this.name = name;
        this.description = description;
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public String getDesc() {
        return description;
    }

    public Integer getId() {
        return id;
    }

    public void setState(String state){
        this.state = state;
    }

    public void setName(String newName){
        this.name = newName;
    }
    public void setDesc(String newDesc){
        this.description = newDesc;
    }

    public String getState(){
        return this.state;
    }

}
