package com.apc1.ontrack.models;

import java.util.ArrayList;
import java.util.List;

public class Board {

    private String name;
    private List<Task> tasks = new ArrayList<Task>();

    public Board(String name){
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public List<Task> getTasks(){
        System.out.println("getting tasks from board "+name);
        System.out.println(tasks.size());
        return tasks;
    }

    public Task addTask(String name, String description){
        Task task = new Task(name, description, getNewTaskId());
        tasks.add(task);
        return task;
    }

    public void removeTask(Integer taskId){
        Task taskToRemove = null;
        for (Task task: tasks){
            if (task.getId().equals(taskId)){
                taskToRemove = task;
            }
        }
        if(taskToRemove!=null){
            tasks.remove(taskToRemove);
        }
    }

    public Task getTask(Integer taskId){
        for (Task task: tasks){
            if (task.getId().equals(taskId)){
                return task;
            }
        }
        return null;
    }

    private Integer getNewTaskId() {
        return tasks.size();
    }

    public String getSummary() {
        String toDoSummary = " :\n";
        String onDoingSummary = " :\n";
        String doneSummary = " :\n";

        int toDoTasksTotal = 0;
        int onDoingTasksTotal = 0;
        int doneTasksTotal = 0;

        for (Task task: tasks){
            if (task.getState().equals("TODO")){
//                toDoTasks.add(task);
                toDoTasksTotal += 1;
                toDoSummary += "\t"+ "◉ " + task.getName() +"\n";
            }else if (task.getState().equals("ONDOING")){
//                onDoingTasks.add(task);
                onDoingTasksTotal += 1;
                onDoingSummary += "\t"+ "◉ " + task.getName() +"\n";
            }else if (task.getState().equals("DONE")){
//                doneTasks.add(task);
                doneTasksTotal += 1;
                doneSummary += "\t"+ "◉ " + task.getName() +"\n";
            }
        }

        //in case there is no task in some specific stated task, it will only skip to a new line
        if (toDoTasksTotal == 0) toDoSummary = ".\n";
        if (onDoingTasksTotal == 0) onDoingSummary = ".\n";
        if (doneTasksTotal == 0) doneSummary = ".\n";

        String summary = "Hey, there! Here is the summary about your "+ name +" board on Ontrack.\n"+
                "You have "+ toDoTasksTotal +" To-Do tasks" +
                toDoSummary +
                "You have "+ onDoingTasksTotal +" On-Doing tasks" +
                onDoingSummary +
                "You have "+ doneTasksTotal +" Done tasks" +
                doneSummary +
                "To get more details, configure, or even make a new summary to your email, click the link below" +
                "that will redirect you to your "+ name +" board\n" +
                "http://ontrack21.herokuapp.com/board/" + name;

        return summary;
    }
}
