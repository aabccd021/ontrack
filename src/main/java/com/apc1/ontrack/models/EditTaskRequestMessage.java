package com.apc1.ontrack.models;

public class EditTaskRequestMessage {
    private String boardName;
    private Integer taskId;
    private String taskDesc;
    private String taskName;

    public EditTaskRequestMessage(){}

    public EditTaskRequestMessage(String boardName, Integer taskId, String taskDesc, String taskName) {
        this.boardName = boardName;
        this.taskId = taskId;
        this.taskDesc = taskDesc;
        this.taskName = taskName;
    }



    public String getBoardName() {
        return boardName;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public String getTaskName() {
        return taskName;
    }
}
