package com.apc1.ontrack.models;


public class AddTaskRequestMessage {
    public String boardName;
    public String taskName;
    public String taskDesc;

    public AddTaskRequestMessage() {
    }

    public AddTaskRequestMessage(String boardName, String taskName, String taskDesc) {
        this.boardName = boardName;
        this.taskName = taskName;
        this.taskDesc = taskDesc;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public String getTaskName() {
        return taskName;
    }

    public String getBoardName() {
        return boardName;
    }
}
