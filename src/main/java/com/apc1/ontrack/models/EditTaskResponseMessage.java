package com.apc1.ontrack.models;

public class EditTaskResponseMessage {
    private String boardName;
    private Integer taskId;
    private String taskDesc;
    private String taskName;


    public EditTaskResponseMessage(Board board, Task task){
        this.boardName = board.getName();
        this.taskId = task.getId();
        this.taskDesc = task.getDesc();
        this.taskName = task.getName();
    }

    public String getBoardName() {
        return boardName;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public String getTaskName() {
        return taskName;
    }
}
