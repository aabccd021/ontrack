package com.apc1.ontrack.models;

public class MoveTaskRequestMessage {
    private String boardName;
    private Integer taskId;
    private String taskState;

    public MoveTaskRequestMessage(){}

    public MoveTaskRequestMessage(String boardName, Integer taskId, String taskState) {
        this.boardName = boardName;
        this.taskId = taskId;
        this.taskState = taskState;
    }

    public String getBoardName() {
        return boardName;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public String getTaskState() {
        return taskState;
    }
}
