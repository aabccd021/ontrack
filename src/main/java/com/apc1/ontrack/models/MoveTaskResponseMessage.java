package com.apc1.ontrack.models;

public class MoveTaskResponseMessage {
    private String boardName;
    private Integer taskId;
    private String taskState;


    public MoveTaskResponseMessage(Board board, Task newTask) {
        this.boardName = board.getName();
        this.taskId = newTask.getId();
        this.taskState = newTask.getState();
    }

    public String getBoardName() {
        return boardName;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public String getTaskState() {
        return taskState;
    }
}
