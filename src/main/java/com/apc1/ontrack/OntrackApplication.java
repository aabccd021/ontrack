package com.apc1.ontrack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OntrackApplication {

    public static void main(String[] args) {
        SpringApplication.run(OntrackApplication.class, args);
    }

}
