package com.apc1.ontrack.services;

import com.apc1.ontrack.models.Board;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BasicService {

    public List<Board> boards;

    BasicService(){
        this.boards = new ArrayList<>();
    }

    public Board getBoard(String name){
        Board board = getBoardFromList(name);
        if (board==null){
            return getNewBoard(name);
        }
        return board;
    }

    private Board getNewBoard(String name){
        System.out.println("get new board");
        Board newBoard = new Board(name);
        boards.add(newBoard);
        return newBoard;
    }

    private Board getBoardFromList(String name){
        for (Board board : boards){
            if (board.getName().equals(name)){
                return board;
            }
        }
        System.out.println("Board Not Found");
        return null;
    }
}
