package com.apc1.ontrack.ws.config;

import com.apc1.ontrack.models.AddTaskRequestMessage;
import com.apc1.ontrack.models.DeleteTaskRequestMessage;
import com.apc1.ontrack.models.EditTaskRequestMessage;
import com.apc1.ontrack.models.MoveTaskRequestMessage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureMockMvc
public class WsConfigIntegrationTest {

    @Value("${local.server.port}")
    private int port;

//    @Autowired
//    private WsProxy wsProxy;

    private WebSocketStompClient stompClient;
    private StompSession stompSession, stompSession2, stompSession3, stompSession4;


    @Before
    public void setUp() throws Exception {
        String wsUrl = "ws://localhost:" + port + "/ontrack-websocket";
        List<Transport> transports = new ArrayList<>(1);
        transports.add(new WebSocketTransport(new StandardWebSocketClient()));
        WebSocketClient transport = new SockJsClient(transports);
        stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        stompSession = stompClient.connect(wsUrl, new WsTestUtils.MyStompSessionHandler()).get();
        stompSession2 = stompClient.connect(wsUrl, new WsTestUtils.MyStompSessionHandler()).get();
        stompSession3 = stompClient.connect(wsUrl, new WsTestUtils.MyStompSessionHandler()).get();
        stompSession4 = stompClient.connect(wsUrl, new WsTestUtils.MyStompSessionHandler()).get();
    }

    @After
    public void tearDown() throws Exception {
        stompSession.disconnect();
        stompSession2.disconnect();
        stompSession3.disconnect();
        stompClient.stop();
    }

    @Test
    public void connectsToSocket() throws Exception {

        assertThat(stompSession.isConnected()).isTrue();
    }

    @Test
    public void addMoveEditRemoveTaskTest() throws Exception {

        CompletableFuture<String> resultKeeper = new CompletableFuture<>();

        stompSession.subscribe(
                "/board/addTaskResponse",
                new WsTestUtils.MyStompFrameHandler((payload) -> resultKeeper.complete(payload.toString())));
        Thread.currentThread().sleep(1000);
        String boardName = "nama board";
        String taskName = "nama task";
        String newTaskName = "nama task baru";

        AddTaskRequestMessage addNew = new AddTaskRequestMessage(boardName, taskName, "deskripsi");
        stompSession.send("/app/addTask", addNew);
        Thread.currentThread().sleep(1000);
        MoveTaskRequestMessage moveTaskCommand = new MoveTaskRequestMessage(boardName, 0, "ONDOING");
        stompSession2.send("/app/moveTask", moveTaskCommand);
        Thread.currentThread().sleep(1000);
        EditTaskRequestMessage editTaskCommand = new EditTaskRequestMessage(boardName, 0, newTaskName, "deskripsi baru");
        stompSession3.send("/app/editTask", editTaskCommand);
        Thread.currentThread().sleep(1000);
        DeleteTaskRequestMessage deleteTaskCommand = new DeleteTaskRequestMessage(boardName, 0);
        stompSession4.send("/app/deleteTask", deleteTaskCommand);
//        assertThat(resultKeeper.get()).isEqualTo("test-payload");
    }


    private List<Transport> createTransportClient() {
        List<Transport> transports = new ArrayList<>(1);
        transports.add(new WebSocketTransport(new StandardWebSocketClient()));
        return transports;
    }


}

