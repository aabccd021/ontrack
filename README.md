```bash
Group       : 1
Class       : ADPRO C
Members     : Edward Partogi Gembira A. (1706979215)
              Ihsan Naufal Ardanto (1406576641)
              Muhamad Abdurahman (1706040095)
              Nurhaya Kushadi Gitasari (1506689194)
              Roshani Ayu Pranasti (1706026052)

```

## Heroku Link
onTrack: A Project Management Application<br>
http://ontrack21.herokuapp.com

## Application Status
[![Pipeline](https://gitlab.com/aabccd021/ontrack/badges/master/pipeline.svg)](https://gitlab.com/aabccd021/ontrack/commits/master)
[![Coverage](https://gitlab.com/aabccd021/ontrack/badges/master/coverage.svg)](https://gitlab.com/aabccd021/ontrack/commits/master)

## Tasks Distribution
1. Edward Partogi Gembira A: Feature of sending user an email that contains the summary of the board
3. Muhamad Abdurahman: Websocket and the feature of moving the card from and to another board
4. Nurhaya Kushadi Gitasari: Test and the feature of creating a new board
5. Roshani Ayu Pranasti: User Interface and the feature of creating, updating, and deleting a card